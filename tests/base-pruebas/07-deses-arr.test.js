import { retornaArreglo } from "../../src/base-pruebas/07-deses-arr"


describe('Pruebas en 07-deses-arr', () => {
    test('debe de retornanr un string y un número', () => {

        // const retorno = retornaArreglo();
        const [letters, numbers] = retornaArreglo();

        expect(letters).toBe('ABC');
        expect(numbers).toBe(123);
        //cuanquiera como esto1
        expect(typeof letters).toBe('string')
        expect(typeof numbers).toBe('number')
        //cualquiera como esto2.
        expect(letters).toEqual(expect.any(String));
    });
});